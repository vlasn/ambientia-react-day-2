import React from "react";
import "./App.css";
import Navbar from "./components/Navbar";
import Router from "./pages";
import TodoContext from "./contexts/todoContext";

function App() {
  const [todos, setTodos] = React.useState([]);
  const getTodoById = (id) => todos.find((todo) => todo.id === id);
  const editTodo = (id, changes) => {
    setTodos((prevTodos) =>
      prevTodos.map((todo) => {
        if (todo.id !== id) return todo;
        return {
          ...todo,
          ...changes,
        };
      })
    );
  };

  return (
    <div className="App">
      <Navbar />
      <section className="content">
        <TodoContext.Provider
          value={{ todos, setTodos, getTodoById, editTodo }}
        >
          <Router />
        </TodoContext.Provider>
      </section>
    </div>
  );
}

export default App;
