import classnames from "classnames";
import React from "react";
import Button from "./Button";
import { Link } from "react-router-dom";
const styles = require("./Todo.module.css");

const Todo = ({ title, content, done, dueTime, onToggleDone, id }) => {
  return (
    <div className={classnames(styles.todo, { [styles.done]: done })}>
      <div className={styles.todoTitle}>
        <Link to={`/todos/${id}`}>{title}</Link>
      </div>
      <p className={styles.todoContent}>{content}</p>
      <Button onClick={onToggleDone}>
        {done ? "Mark undone" : "Mark done"}
      </Button>
    </div>
  );
};

export default Todo;
