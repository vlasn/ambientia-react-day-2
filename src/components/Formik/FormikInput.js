import React from "react";
import Input from "../Input";

const FormikInput = ({ formik, name, label, type = "text" }) => {
  return (
    <Input
      label={label}
      type={type}
      error={formik.touched[name] && formik.errors[name]}
      value={formik.values[name]}
      {...formik.getFieldProps(name)}
    />
  );
};

export default FormikInput;
