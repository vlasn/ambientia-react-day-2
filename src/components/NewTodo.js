import React from "react";
import Input from "./Input";
import Button from "./Button";

function NewTodo({ onSubmit }) {
  const [formState, setFormState] = React.useState({ title: "", content: "" });

  const handleFieldChange = (fieldName, value) => {
    setFormState((prevState) => ({ ...prevState, [fieldName]: value }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit?.(formState);
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <Input
          onChange={(e) => handleFieldChange("title", e.target.value)}
          label="title"
          name="title"
          placeholder="title"
          value={formState.title}
        />
        <Input
          onChange={(e) => handleFieldChange("content", e.target.value)}
          label="content"
          name="title"
          placeholder="content"
          value={formState.content}
        />
        <Button>Submit</Button>
      </form>
    </div>
  );
}

export default NewTodo;
