import React from "react";

const fruits = ["avocado", "banana", "watermelon", "kiwi", "strawberry"];

function RenderingListsSample() {
  return (
    <div>
      {fruits.map((fruit, index) => {
        return <div key={fruit}>{`${index + 1}. ${fruit}`}</div>;
      })}
    </div>
  );
}

export default RenderingListsSample;
