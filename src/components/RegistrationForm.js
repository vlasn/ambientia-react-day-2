import React from "react";
import Button from "./Button";
import { useFormik } from "formik";
import * as Yup from "yup";
import FormikInput from "./Formik/FormikInput";

Yup.addMethod(Yup.mixed, "sameAs", function (ref, message) {
  return this.test("sameAs", message, function (value) {
    const other = this.resolve(ref);
    return value === other;
  });
});

const registrationFormSchema = Yup.object({
  username: Yup.string()
    .min(3, "Username length needs to be at least 3 characters")
    .max(16, "Username length needs to be 16 characters or less")
    .required("Username is required!"),
  password: Yup.string()
    .min(8, "Password length needs to be at least 8 characters")
    .max(255, "Password length needs to be 255 c3haracters or less")
    .matches(/[0-9]/, "Password needs to contain at least one number!")
    .matches(/[a-zA-Z]/, "Password needs to contain at least one letter!")
    .required("Password is required!"),
  confirmPassword: Yup.mixed()
    .sameAs(Yup.ref("password"), "Passwords need to match")
    .required("Confirming your password is required!"),
  email: Yup.string().email("Email is invalid").required("Email is required!"),
});

const RegistrationForm = () => {
  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
      confirmPassword: "",
      email: "",
    },
    validationSchema: registrationFormSchema,
    onSubmit: (values) => {
      console.log({ values });
      console.log("submit some data");
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <FormikInput
        label="Username"
        name="username"
        type="text"
        formik={formik}
      />
      <FormikInput
        label="Password"
        name="password"
        type="password"
        formik={formik}
      />
      <FormikInput
        label="Confirm password"
        name="confirmPassword"
        type="password"
        formik={formik}
      />
      <FormikInput label="Email" name="email" type="email" formik={formik} />
      <Button type="submit">Submit</Button>
    </form>
  );
};

export default RegistrationForm;
