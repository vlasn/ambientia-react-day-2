import classnames from "classnames";
import React from "react";
const styles = require("./Button.module.css");

export default function Button({
  value,
  children,
  variant,
  onClick,
  ghost,
  type,
}) {
  return (
    <button
      className={classnames(styles.button, styles[variant] || null, {
        [styles.ghost]: ghost,
      })}
      onClick={onClick}
      type={type || "submit"}
    >
      {children || value}
    </button>
  );
}
