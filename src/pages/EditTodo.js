import React from "react";
import { useParams, useNavigate } from "react-router-dom";
import Input from "../components/Input";
import Button from "../components/Button";
import TodoContext from "../contexts/todoContext";

const EditTodo = () => {
  const { id } = useParams();
  const { getTodoById, editTodo } = React.useContext(TodoContext);
  const navigate = useNavigate();

  const todo = React.useMemo(() => {
    const todo = getTodoById(id);
    return todo;
  }, [id, getTodoById]);

  React.useEffect(() => {
    if (!todo) navigate("/");
  }, [todo, navigate]);

  const handleClick = React.useCallback(() => {
    editTodo(id, { done: !todo.done });
  }, [editTodo, todo, id]);

  const handleFieldChange = React.useCallback(
    (event) => {
      editTodo(id, { [event.target.name]: event.target.value });
    },
    [editTodo, id]
  );

  return (
    <div>
      <Input name="title" value={todo?.title} onChange={handleFieldChange} />
      <Input
        name="content"
        value={todo?.content}
        onChange={handleFieldChange}
      />
      <Button onClick={handleClick}>
        {todo?.done ? "Mark undone" : "Mark done"}
      </Button>
    </div>
  );
};

export default EditTodo;
