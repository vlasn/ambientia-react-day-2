import React from "react";
import { v4 } from "uuid";
import TodoContext from "../contexts/todoContext";

import NewTodo from "../components/NewTodo";

import Todo from "../components/Todo";

const Todos = () => {
  const { todos, setTodos, editTodo } = React.useContext(TodoContext);

  const undoneTodoCount = React.useMemo(() => {
    return todos.filter((todo) => !todo.done).length;
  }, [todos]);

  React.useEffect(() => {
    document.title = `${undoneTodoCount} undone todos`;
  }, [undoneTodoCount]);

  const onNewTodo = React.useCallback(
    (todo) => {
      const newTodo = {
        ...todo,
        id: v4(),
        done: false,
      };
      setTodos((prevState) => [...prevState, newTodo]);
    },
    [setTodos]
  );

  const toggleTodoDone = (id, done) => {
    editTodo(id, { done });
  };

  const renderTodo = ({ title, content, done, id }) => {
    return (
      <Todo
        onToggleDone={() => toggleTodoDone(id, !done)}
        key={id}
        id={id}
        title={title}
        content={content}
        done={done}
      />
    );
  };
  return (
    <div>
      <NewTodo onSubmit={onNewTodo} />
      <div className="todos">{todos.map(renderTodo)}</div>
    </div>
  );
};

export default Todos;
